drop database if exists EDW_STG;
create database EDW_STG clone EDW_DEV;
grant usage on database EDW_STG to role SVC_RO_DEV_ROLE;
grant usage on database EDW_STG to role SVC_RW_DEV_ROLE;
